#! /usr/bin/bash

stack='LLM-RAG'
env='prod'
code_resources_s3_prefix='code_resources'

aws cloudformation deploy --template-file cfn/init_stack.yml --stack-name $stack-InitStack --capabilities CAPABILITY_NAMED_IAM --parameter-overrides Env=prod

s3_bucket_name=$(aws cloudformation describe-stacks --stack-name $stack-InitStack --query "Stacks[0].Outputs[?OutputKey=='S3Bucket'].OutputValue" --output text)

aws s3 cp notebooks s3://$s3_bucket_name/notebooks --recursive

cd layers/python

for folder in */; do
    cd $folder
    python3.10 -m venv "python"
    source python/bin/activate
    pip install -r requirements.txt
    deactivate
    zip -r layer.zip python/ -x "python/bin/*" "python/.gitignore" "python/pyenv.cfg"
    rm -rf python
    cd ..
done

cd ../..

aws cloudformation package --template-file cfn/compute_stack.yml --s3-bucket $s3_bucket_name --s3-prefix $code_resources_s3_prefix --output-template-file cfn/compute_stack_packaged.yml

aws s3 cp cfn/compute_stack_packaged.yml s3://$s3_bucket_name/cfts/
aws s3 cp cfn/neptune_base_stack.json s3://$s3_bucket_name/cfts/
rm -rf cfn/compute_stack_packaged.yml

cd layers/python

for folder in */; do
    cd $folder    
    rm -rf layer.zip
    cd ..
done

cd ../..

aws cloudformation deploy --template-file cfn/neptune_stack.yml --stack-name $stack-NeptuneStack --capabilities CAPABILITY_NAMED_IAM --parameter-overrides Env=$env S3Bucket=$s3_bucket_name --disable-rollback

aws cloudformation wait stack-create-complete --stack-name $stack-NeptuneStack

sleep 10m

resources_s3_bucket_name=$(aws cloudformation describe-stacks --stack-name $stack-NeptuneStack --query "Stacks[0].Outputs[?OutputKey=='ResourcesS3Bucket'].OutputValue" --output text)

for folder in graphs/*/; do
    cd $folder
    python run.py    
    cd ../..
    ttl_file=$(find "$folder" -type f -name "*.ttl")
    aws s3 cp $ttl_file s3://$resources_s3_bucket_name/ttls/
done

