#! /usr/bin/bash

stack='LLM-RAG'
s3_bucket_name=$(aws cloudformation describe-stacks --stack-name $stack-InitStack --query "Stacks[0].Outputs[?OutputKey=='S3Bucket'].OutputValue" --output text)
resources_s3_bucket_name=$(aws cloudformation describe-stacks --stack-name $stack-NeptuneStack --query "Stacks[0].Outputs[?OutputKey=='ResourcesS3Bucket'].OutputValue" --output text)
aws s3 rm s3://$s3_bucket_name --recursive
aws s3 rm s3://$resources_s3_bucket_name --recursive
aws cloudformation delete-stack --stack-name $stack-InitStack
aws cloudformation delete-stack --stack-name $stack-NeptuneStack