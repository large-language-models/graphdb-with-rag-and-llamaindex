#! /usr/bin/bash

stack='LLM-RAG'

echo -e "Enter folder name: "
read folder_name

resources_s3_bucket_name=$(aws cloudformation describe-stacks --stack-name $stack-NeptuneStack --query "Stacks[0].Outputs[?OutputKey=='ResourcesS3Bucket'].OutputValue" --output text)

echo "Resources upload bucket name is : $resources_s3_bucket_name"

cd graphs/$folder_name
python run.py
cd ../..
ttl_file=$(find "graphs/$folder_name" -type f -name "*.ttl")
aws s3 cp $ttl_file s3://$resources_s3_bucket_name/ttls/
