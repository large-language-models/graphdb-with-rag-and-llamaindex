#! /usr/bin/bash

stack='LLM-RAG'
env='prod'
code_reources_s3_prefix='code_reources/dev'

echo -e "Enter folder name: "
read folder_name
echo -e "Enter libraries to install: "
read library_names

s3_bucket_name=$(aws cloudformation describe-stacks --stack-name $stack-InitStack --query "Stacks[0].Outputs[?OutputKey=='S3Bucket'].OutputValue" --output text)

# Creates the directory if it doesn't exist
mkdir -p layers/python/$folder_name

cd layers/python/$folder_name

python3.10 -m venv "python"
source python/bin/activate

requirements_file=requirements.txt
if test -f "$requirements_file"; then
    pip install -r $requirements_file
fi

IFS=' ' read -ra library_names_array <<< "$library_names"
for library in "${library_names_array[@]}"
do
    IFS='==' read -ra library_name_array <<< "$library"
    pip show ${library[0]} 1>/dev/null #pip for Python 2
    if [ $? == 0 ]; then
        echo "Uninstalling ${library[0]}"
        pip uninstall $library 
    fi
done

pip install $library_names
pip freeze > requirements.txt
deactivate
zip -r ${folder_name}.zip python/ -x "python/bin/*" "python/.gitignore" "python/pyenv.cfg"
rm -rf python

cd ../../..

aws s3 cp layers/python/$folder_name/${folder_name}.zip s3://$s3_bucket_name/$code_reources_s3_prefix/

rm -rf layers/python/$folder_name/${folder_name}.zip

