#! /usr/bin/bash


stack='LLM-RAG'
env='prod'
code_resources_s3_prefix='code_resources'
s3_model_repo='global_model_repo'

resources_s3_bucket_name=$(aws cloudformation describe-stacks --stack-name $stack-NeptuneStack --query "Stacks[0].Outputs[?OutputKey=='ResourcesS3Bucket'].OutputValue" --output text)

subnet_id=$(aws cloudformation describe-stacks --stack-name $stack-NeptuneStack --query "Stacks[0].Outputs[?OutputKey=='Subnet1'].OutputValue" --output text)

sg_id=$(aws cloudformation describe-stacks --stack-name $stack-NeptuneStack --query "Stacks[0].Outputs[?OutputKey=='NeptuneSG'].OutputValue" --output text)

# hf_token=$(cat secrets/prod.yml | yq '.huggingface_token')

aws cloudformation deploy --template-file cfn/llm_deploy_stack.yml --stack-name $stack-LLMDeployStack --capabilities CAPABILITY_NAMED_IAM --parameter-overrides Env=$env LLMModelUri=$s3_model_repo ResourcesUploadBucket=$resources_s3_bucket_name NeptuneSubnet1=$subnet_id NeptuneSG=$sg_id