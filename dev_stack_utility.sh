#! /usr/bin/bash

# Access and use the arguments
echo -e "Welcome to the DEV stack interaction utility. With this utility, you can do the below activities.\n 1 - Create a specific layer\n 2 - Create a specific lambda\n 0 - Bring Your Own Script (Modify scripts/test.sh)\n Please provide the number corresponding to the activity you would like to execute: "

read user_input

mkdir -p logs/dev

if [ "$user_input" -eq 1 ]; then
    echo "START: Creating Layer"
    ./scripts/dev/create_layer.sh
    echo "END: Layer Creation completed"
elif [ "$user_input" -eq 2 ]; then
    echo "Placeholder for creating lambda"
elif [ "$user_input" -eq 0 ]; then
    echo "START: Running test script"
    ./scripts/dev/test.sh > logs/dev/test.log 2>&1
    echo "END: Test script executed"
else
    echo "Please provide a correct input"
fi
