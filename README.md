# RAG Complete Cloudformation Stack with Neptune Graph

This is a one-click provisioning and deployment solution which is aimed towards setting up a complete LLM RAG stack. The source code deployment and cloudformation resources are all logically organized in the same repo so that one who is looking to quickly setup a complete RAG-LLM stack can do so with just one-click rather than having to go through the overhead of setting up and configuring multiple repositories.

With this setup, the user just have to place the owlready2 ontology python code in the `graphs` folder and everything from the generation of OWL file to the genration of TTL file to the provisioning and automatic loading of the graph onto a Neptune cluster is taken care of by the stack creation process. Any accompanying lambda code and layer code can be placed in the `lambdas` and `layers` folders respectively and placing the corresponding cloudformation template in the `compute_stack.yml` file will deploy these resources as well. Just give the option `1` while running `./stack_utility.sh` and you are good to go.

The below AWS resources along with their deployment code are setup as part of the single-click stack creation process. (More resources coming soon).

1. AWS Neptune Graph 
2. Sagemaker Notebook running on top of the Neptune cluster
3. OWL and Turtle files from an owlready2 ontology using an accompanying `data.csv`
4. Automatically loading the above generated ttl file into the Neptune DB cluster via a Lambda function.
5. A standalone lambda function for fetching data from the loaded graph using SPARQL queries
6. Lambda layers for the lambda functions

## Setup

If the workspace is opened in a gitpod environment, all the necessary init commands will be automatically run at the startup itself using the `.gitpod.yml` file. In AWS Cloud9, execute the command `source c9init.sh` to prime up the environment. In all other UNIX environment, the commands specified in the `.gitpod.yml` file will have to be run manually. If AWS cli is already installed in your systems, the first 6 init commands from the `.gitpod.yml` file can be skipped.

Once the init commands are completed, you have to configure AWS cli using the command `aws configure --profile service-user`. Provide the credentials corresponding to a CLI-only user configured for your account and the region as `us-east-1` and leave the Default output format as empty itself. After the profile is configured, activate this profile by issuing the command `export AWS_PROFILE=service-user`. To view the available profiles, run `aws configure list-profiles` and to view the current default profile `aws configure list`. 

## Run

### Main utility

To setup and interact with the individual stack resources, a bash script named `stack_utility` is placed at the root folder. To setup the stack, run `./stack_utility.sh` and follow the on-screen instructions. The option for setting up the stack is `1` and delete the stack is `5`. 

### Dev utility

To push zip folders for custom layers onto AWS S3, run the option `1` from the `./dev_stack_utility.sh`. The tool would prompt for the name of the folder where the requirements file for the layer is maintained (created the folder if it not exists). After providing the folder name, it would ask which all libraries need to be installed in the layer. Just providing the name of the libraries is enough (libraries with specific version numbers is also supported). Hitting enter, would create the zipped layer folder and push it to an S3 bucket.

If you would like to run test scripts, place your test script into `scripts/dev/test.sh` and run the `dev_stack_utility` with the option `0`.

## Stack Architecture

Since cloudformation is concerned with only the provisioning of resources and not code deployment, I have segregated the templates into two: first one for setting up the initial code artifacts hosting resources (Sagemaker Notebooks, zipped lambdas and layers, AWS packaged CFTs) using `init_stack.yml` and the second one for deploying Neptune cluster, S3 based lambdas and other resources using `neptune_stack.yml`. This segregation is required because after the creation of the first stack, the lambda and layer code to be deployed is packaged and pushed to S3 created as part of the first stack. Once the code for lambdas and layers is available as S3 objects in the aforementioned stack, the stack for deploying the Neptune cluster, lambdas, etc will get triggered. The `neptune_stack.yml` is comprised of mainly two nested stacks: one for the actual Neptune cluster deployment (which in turn have nested stacks) named `NeptuneStack` and the other one named `ComputeStack` for deploying lambdas, layers, other S3s, etc.

NOTE: All resources which require the code artifacts to be present beforehand as S3 artifacts uploaded either via `aws cli` or using `aws package` command need to defined in the `cfn/compute_stack.yml` file only, which gets called as a nested stack in the `neptune_stack.yml` file.

## Adding Resources

### Lambdas & Layers

1. Place the lambda code as a folder inside `lambdas` folder
2. [OPTIONAL] Create a new folder and put the `requirements.txt` inside it and place the newly created folder in `layers/python` folder
3. Add the corresponding logical item for the lambda and layer inside `cfn/compute_stack.yml`. Connect the layer with the lambda in the logical CFT stack item (if needed).

### Graphs

1. Create a new folder for a new graph ontology inside `graphs` folder.
2. The folder should have `run.py` script which gets executed during the creation of the stack.
3. Follow along the lines of the `coffee` ontology for setting up a new ontology in a separate folder. In `coffee` ontology, the owlready2 ontology is defined in `coffee.py`. Running the `run.py` script picks up this `coffee.py` script and creates `coffee.owl` file and subsequently `coffee.ttl` file using the `data.csv` data file. This 2 step file generation is required because owlready2 library cannot directly write `.ttl` files. Therefore, using owlready2, `.owl` file is generated. Further, using the library `rdflib`, `.ttl` file is created using the generated `.owl` file and the data file.
4. Any libraries required for the `.ttl` file generation can be installed in the local `llm_env` environment itself, as the `run.py` script is run in the local environment only and not in the cloud environment. If any such libraries are installed, remember to save the newly added libraries onto the local `requirements.txt` file using the command `pip freeze > requirements.txt`. 
5. After the code artifacts are correctly placed in the repo, run the option `6` in the `stack_utility.sh`. This will generate the `.ttl` file, upload to S3 and will get automatically loaded into the Neptune DB.

NOTE: The generated `.ttl` file gets moved to an S3 bucket in the created stack, from there it can be loaded onto the Graph DB using a Sagemaker Notebook running on top of the Neptune cluster created as part of the stack. An example for doing so is provided in the jupyter notebook, `Neptune_Ontology_Example.ipynb`. This notebook also gets copied onto the Sagemaker instance during the stack creation phase.

## Notes

1. The `aws_helpers_layer` holds the dependencies for the general AWS and python functionalities including an AWS lambda environment `urllib` compatible `requests` library. All the general dependencies need to be maintained here.
2. Do not change the order of the resources in the stack files. New resources can be added in between the existing resources but the occurence order of the existing resources should not be disturbed.

## Reference Codes

1. Example: Connecting to Neptune Using Python with Signature Version 4 Signing - `reference_codes/neptunesigv4.py`

## Useful Resources

### Stack Setup 

1. [IMPORTANT] Python AIO code from AWS for loading, querying etc across various endpoints like `/sparql`, `/gremlin`, `loader`, etc - `https://docs.aws.amazon.com/neptune/latest/userguide/iam-auth-connecting-python.html`
2. Building Ontologies with Python - `https://paul-bruffett.medium.com/building-ontologies-with-python-84238d6eee52`
3. Cloudformation Playlist - `https://www.youtube.com/watch?v=fc6tfw2tcGE&list`
4. AWS blog on Lambda deployment with CustomResource - `https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-lambda-function-code-cfnresponsemodule.html`
5. Medium blog on Lambda deployment with CustomResource - `https://stanislav-saprankov.medium.com/run-scripts-on-cloudformation-stack-creation-with-custom-resources-258a37ecdf75`
6. AWS blog on Model Driven graphs using OWL in Neptune - `https://aws.amazon.com/blogs/database/model-driven-graphs-using-owl-in-amazon-neptune/`
7. Accompanying Git repo to the above mentioned blog - `https://github.com/aws-samples/amazon-neptune-ontology-example-blog/tree/main`
8. Lambda with S3 - `https://docs.aws.amazon.com/lambda/latest/dg/with-s3-example.html`
9. Neptune Load - `https://docs.aws.amazon.com/neptune/latest/userguide/bulk-load-data.html`
10. Neptune Loader Command - `https://docs.aws.amazon.com/neptune/latest/userguide/load-api-reference-load.html#load-api-reference-load-parameters`

### LLM Setup

1. Deploying LLama2 on AWS SageMaker - `https://www.philschmid.de/sagemaker-llama-llm`
2. Secure Deployment of LLama2 on AWS SageMaker VPC - `https://www.philschmid.de/sagemaker-llm-vpc`
3. LLMOps Deployment of LLM with CDK - `https://www.philschmid.de/cdk-llama2`
4. LLM Posts from Phil Schmid (All of the above are from Phil Schmid) - `https://www.philschmid.de/tags/llm`
5. Different ways of Huugingface LLM models on AWS - `https://medium.com/technology-nineleaps/deploying-the-hugging-face-llm-in-amazon-sagemaker-37b71cd74aca`
6. Request Quota for instances on AWS - `https://adithyask.medium.com/deploy-mistral-llama-7b-on-aws-in-10-mins-cc80e88d13f2`
7. Download/Access LLama2 - `https://ai.meta.com/llama/get-started/`

## Error Fixes

1. Unable to validate the following destination configurations - `https://www.itonaut.com/2018/10/03/implement-s3-bucket-lambda-triggers-in-aws-cloudformation/`
2. Delete contents of S3 bucket to avoid error during stack deletion - `https://medium.com/@bhavyalathab/aws-cloud-formation-deleting-non-empty-s3-bucket-created-with-formation-template-c70292785e1b`
3. Module incompatibility between requests and urllib3 - `https://stackoverflow.com/questions/76414514/cannot-import-name-default-ciphers-from-urllib3-util-ssl-on-aws-lambda-us`