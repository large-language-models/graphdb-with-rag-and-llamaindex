from owlready2 import *

#we define our ontology; all ontologies share the same world by default
# The URL provided here is a dummy URL
onto = get_ontology("http://test.org/coffee.owl")

with onto:
    #our entities are classes
    class Coffee(Thing): pass
    #related information can also be captured as classes
    class Roast(Thing): pass
    
    #subclassing Roast to break down additional details
    class Dark_Roast(Roast): pass
    class Blonde_Roast(Roast): pass
    class Medium_Roast(Roast): pass
    
    
    class Region(Thing): pass
    
    class Latin_America(Region): pass
    class Asia_Pacific(Region): pass
    class Multi(Region): pass

    class Processing(Thing): pass
    
    class Washed(Processing): pass
    class Semi_Washed(Processing): pass

    #defining the relationship between coffee and roast
    class has_roast(ObjectProperty, FunctionalProperty):
        domain = [Coffee]
        region = [Roast]
    
    
    #FunctionalProperties mean it can only be related to one; these coffees can only be grown in one region
    class from_region(Coffee >> Region, FunctionalProperty):
        pass

    #inverse relationships indicate the relationship is bi-directional
    class grown_in(Region >> Coffee):
        inverse = from_region

    class has_body(Coffee >> str, FunctionalProperty):
        pass
    
    class has_processing(Coffee >> Processing, ObjectProperty):
        pass

    #defining the characteristics for a specific coffee type or line
    #.some means it must be related to a Region
    #.only means if it's related to a region it must be the one defined
    class Veranda(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Blonde_Roast) &
                         from_region.some(Region) & 
                        from_region.only(Latin_America) & has_body.value("Light") 
                         & has_processing.some(Processing) & has_processing.only(Washed)]

    class Pike(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Medium_Roast) &
                         from_region.some(Region) &
                        from_region.only(Latin_America) & has_body.value("Medium") &
                         has_processing.some(Processing) & has_processing.only(Washed)]

    class Sumatra(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Dark_Roast) &
                         from_region.some(Region) &
                        from_region.only(Asia_Pacific) & has_body.value("Full") &
                        has_processing.some(Processing) & has_processing.only(Washed)]
        
    #we can have two washes for this coffee, washed or semi-washed
    class Yukon_Blend(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Medium_Roast) &
                        from_region.some(Region) & from_region.only(Multi) &
                        has_body.value("Full") & has_processing.some(Processing) &
                        has_processing.only(Washed | Semi_Washed)]

AllDisjoint([Dark_Roast, Blonde_Roast, Medium_Roast])
AllDifferent([Latin_America, Asia_Pacific, Multi])

latam = Latin_America()
apac = Asia_Pacific()
multi = Multi()

#defining some unknown coffees and their characteristics
coffee1 = Coffee(has_roast = Blonde_Roast(), from_region=latam, has_body = "Light",has_processing=[Washed()])
coffee2 = Coffee(has_roast = Dark_Roast(), from_region=apac, has_body = "Full", has_processing=[Washed()])
coffee3 = Coffee(has_roast = Medium_Roast(), from_region=multi, has_body = "Full", has_processing=[Washed(),Semi_Washed()])

#closing the world, by default we have an open world, anything not restricted is possible
close_world(Coffee)

sync_reasoner()

print("Coffee 1 is: ", coffee1.is_a)
print("Coffee 1 class is: ", coffee1.__class__)
print("Coffee 2 is: ", coffee2.is_a)
print("Coffee 2 class is: ", coffee2.__class__)
print("Coffee 3 is: ", coffee3.is_a)
print("Coffee 3 class is: ", coffee3.__class__)
print("Coffee is: ", Coffee.is_a)
print("Pike is: ", Pike.is_a)
print("Coffee grown in Latam: ", latam.grown_in)
print("Properties of coffee1: ", coffee1.get_properties())

onto.save(file = "graphs/owls/coffee.owl", format = "rdfxml")