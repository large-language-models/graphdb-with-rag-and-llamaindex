from coffee import *
from rdflib import Graph
import csv

def parse_csv(file_path):
    data = []
    with open(file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append(row)
    return data

def create_graph_from_csv(csv_file):
    with onto:
        csv_data = parse_csv(csv_file)
        for row in csv_data:
            coffee = Coffee(name=row['Name'])
            roast = row['Roast']
            if roast == 'D':
                roast_type = Dark_Roast()
            elif roast == 'B':
                roast_type = Blonde_Roast()
            elif roast == 'M':
                roast_type = Medium_Roast()
            else:
                roast_type = Roast()
            coffee.has_roast = roast_type

            region = row['Region']
            if region == 'L':
                region_type = Latin_America()
            elif region == 'A':
                region_type = Asia_Pacific()
            elif region == 'M':
                region_type = Multi()
            else:
                region_type = Region()
            coffee.from_region = region_type

            coffee.has_body = row['Body']

            processing = row['Processing']
            for processing_type in list(processing):
                if processing_type == 'W':
                    processing = Washed()
                elif processing_type == 'S':
                    processing = Semi_Washed()
                else:
                    processing = Processing()
                coffee.has_processing.append(processing)

            coffee.hasUniqueID = row['ID']

    # Save the ontology only if it has content
    if onto.individuals():
        # owlready2 cannot create ttl file, hence, create .owl file first
        onto.save(file="coffee.owl", format="rdfxml")
        graph = Graph()
        graph.parse("coffee.owl")
        turtle_data = graph.serialize(format='turtle')    
        with open('coffee.ttl', 'w') as file:
            file.write(turtle_data)
    else:
        print("Ontology is empty, not saving as Turtle file")

if __name__ == "__main__":
    create_graph_from_csv("data.csv")