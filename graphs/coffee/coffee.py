from owlready2 import *

#we define our ontology; all ontologies share the same world by default
# The URL provided here is a dummy URL
onto = get_ontology("http://test.org/coffee.owl")

with onto:
    #our entities are classes
    class Coffee(Thing): pass
    #related information can also be captured as classes
    class Roast(Thing): pass
    
    #subclassing Roast to break down additional details
    class Dark_Roast(Roast): pass
    class Blonde_Roast(Roast): pass
    class Medium_Roast(Roast): pass
    
    
    class Region(Thing): pass
    
    class Latin_America(Region): pass
    class Asia_Pacific(Region): pass
    class Multi(Region): pass

    class Processing(Thing): pass
    
    class Washed(Processing): pass
    class Semi_Washed(Processing): pass

    #defining the relationship between coffee and roast
    class has_roast(ObjectProperty, FunctionalProperty):
        domain = [Coffee]
        region = [Roast]
    
    
    #FunctionalProperties mean it can only be related to one; these coffees can only be grown in one region
    class from_region(Coffee >> Region, FunctionalProperty):
        pass

    #inverse relationships indicate the relationship is bi-directional
    class grown_in(Region >> Coffee):
        inverse = from_region

    class has_body(Coffee >> str, FunctionalProperty):
        pass
    
    class has_processing(Coffee >> Processing, ObjectProperty):
        pass

    #defining the characteristics for a specific coffee type or line
    #.some means it must be related to a Region
    #.only means if it's related to a region it must be the one defined
    class Veranda(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Blonde_Roast) &
                         from_region.some(Region) & 
                        from_region.only(Latin_America) & has_body.value("Light") 
                         & has_processing.some(Processing) & has_processing.only(Washed)]

    class Pike(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Medium_Roast) &
                         from_region.some(Region) &
                        from_region.only(Latin_America) & has_body.value("Medium") &
                         has_processing.some(Processing) & has_processing.only(Washed)]

    class Sumatra(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Dark_Roast) &
                         from_region.some(Region) &
                        from_region.only(Asia_Pacific) & has_body.value("Full") &
                        has_processing.some(Processing) & has_processing.only(Washed)]
        
    #we can have two washes for this coffee, washed or semi-washed
    class Yukon_Blend(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Medium_Roast) &
                        from_region.some(Region) & from_region.only(Multi) &
                        has_body.value("Full") & has_processing.some(Processing) &
                        has_processing.only(Washed | Semi_Washed)]

    AllDisjoint([Dark_Roast, Blonde_Roast, Medium_Roast])
    AllDifferent([Latin_America, Asia_Pacific, Multi])

