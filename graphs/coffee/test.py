from owlready2 import *
from rdflib import Graph
import csv

def parse_csv(file_path):
    data = []
    with open(file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append(row)
    return data

#we define our ontology; all ontologies share the same world by default
# The URL provided here is a dummy URL
onto = get_ontology("http://test.org/onto.owl")

with onto:
    #our entities are classes
    class Coffee(Thing): pass
    #related information can also be captured as classes
    class Roast(Thing): pass
    
    #subclassing Roast to break down additional details
    class Dark_Roast(Roast): pass
    class Blonde_Roast(Roast): pass
    class Medium_Roast(Roast): pass
    
    
    class Region(Thing): pass
    
    class Latin_America(Region): pass
    class Asia_Pacific(Region): pass
    class Multi(Region): pass

    class Processing(Thing): pass
    
    class Washed(Processing): pass
    class Semi_Washed(Processing): pass

    #defining the relationship between coffee and roast
    class has_roast(ObjectProperty, FunctionalProperty):
        domain = [Coffee]
        region = [Roast]
    
    
    #FunctionalProperties mean it can only be related to one; these coffees can only be grown in one region
    class from_region(Coffee >> Region, FunctionalProperty):
        pass

    #inverse relationships indicate the relationship is bi-directional
    class grown_in(Region >> Coffee):
        inverse = from_region

    class has_body(Coffee >> str, FunctionalProperty):
        pass
    
    class has_processing(Coffee >> Processing, ObjectProperty):
        pass

    #defining the characteristics for a specific coffee type or line
    #.some means it must be related to a Region
    #.only means if it's related to a region it must be the one defined
    class Veranda(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Blonde_Roast) &
                         from_region.some(Region) & 
                        from_region.only(Latin_America) & has_body.value("Light") 
                         & has_processing.some(Processing) & has_processing.only(Washed)]

    class Pike(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Medium_Roast) &
                         from_region.some(Region) &
                        from_region.only(Latin_America) & has_body.value("Medium") &
                         has_processing.some(Processing) & has_processing.only(Washed)]

    class Sumatra(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Dark_Roast) &
                         from_region.some(Region) &
                        from_region.only(Asia_Pacific) & has_body.value("Full") &
                        has_processing.some(Processing) & has_processing.only(Washed)]
        
    #we can have two washes for this coffee, washed or semi-washed
    class Yukon_Blend(Coffee):
        equivalent_to = [Coffee & has_roast.some(Roast) & has_roast.only(Medium_Roast) &
                        from_region.some(Region) & from_region.only(Multi) &
                        has_body.value("Full") & has_processing.some(Processing) &
                        has_processing.only(Washed | Semi_Washed)]

    AllDisjoint([Dark_Roast, Blonde_Roast, Medium_Roast])
    AllDifferent([Latin_America, Asia_Pacific, Multi])

    csv_data = parse_csv("data.csv")
    for row in csv_data:
        coffee = Coffee(name=row['Name'])
        roast = row['Roast']
        if roast == 'D':
            roast_type = Dark_Roast()
        elif roast == 'B':
            roast_type = Blonde_Roast()
        elif roast == 'M':
            roast_type = Medium_Roast()
        else:
            roast_type = Roast()
        coffee.has_roast = roast_type

        region = row['Region']
        if region == 'L':
            region_type = Latin_America()
        elif region == 'A':
            region_type = Asia_Pacific()
        elif region == 'M':
            region_type = Multi()
        else:
            region_type = Region()
        coffee.from_region = region_type

        coffee.has_body = row['Body']

        processing = row['Processing']
        for processing_type in list(processing):
            if processing_type == 'W':
                processing = Washed()
            elif processing_type == 'S':
                processing = Semi_Washed()
            else:
                processing = Processing()
            coffee.has_processing.append(processing)

coffee_types = list(onto.Coffee.instances())

for coffee in coffee_types:
    print("===================")
    print(f"Coffee Type: {coffee.name}")
    print(f"Roast Type: {coffee.has_roast}")
    print(f"From Region: {coffee.from_region}")
    print(f"Has body: {coffee.has_body}")
    print(f"Has processing: {coffee.has_processing}")
    print("===================")


onto.save(file="coffee.owl", format="rdfxml")

# Create an rdflib Graph and serialize the ontology to Turtle format
graph = Graph()
graph.parse("coffee.owl")
turtle_data = graph.serialize(format='turtle')

# Save the Turtle data to a file
with open('coffee.ttl', 'w') as file:
    file.write(turtle_data)
    
