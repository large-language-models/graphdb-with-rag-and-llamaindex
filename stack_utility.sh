#! /usr/bin/bash

# Access and use the arguments
echo -e "Welcome to the stack interaction utility. With this utility, you can do the below activities.\n 1 - Create the Stack\n 2 - Describe the Stack\n 3 - Start the Neptune Cluster (if stopped)\n 4 - Stop the Neptune Cluster\n 5 - Delete the Stack\n 6 - Load an ontology\n Please provide the number corresponding to the activity you would like to execute: "

read user_input

mkdir -p logs/prod

if [ "$user_input" -eq 1 ]; then
    echo "START: Creating Stack"
    ./scripts/prod/create_stack.sh > logs/prod/create_stack.log 2>&1
    echo "END: Creation completed"
elif [ "$user_input" -eq 2 ]; then
    echo "START: Describing Stack"
    ./scripts/prod/describe_stack.sh > logs/prod/describe_stack.log 2>&1
    echo "END: Described Stack. Please check the logs at logs/prod/describe_stack.log"
elif [ "$user_input" -eq 3 ]; then
    echo "START: Starting Neptune Cluster"
    ./scripts/prod/start_neptune.sh > logs/prod/start_neptune.log 2>&1
    echo "END: Neptune Cluster started"
elif [ "$user_input" -eq 4 ]; then
    echo "START: Stopping Neptune Cluster"
    ./scripts/prod/stop_neptune.sh > logs/prod/stop_neptune.log 2>&1
    echo "END: Neptune Cluster stopped"
elif [ "$user_input" -eq 5 ]; then
    echo "START: Deleting Stack"
    ./scripts/prod/delete_stack.sh > logs/prod/delete_stack.log 2>&1
    echo "END: Stack Deleted"
elif [ "$user_input" -eq 6 ]; then
    echo "START: Loading Ontology"
    ./scripts/prod/load_ontology.sh
    echo "END: Ontology loaded"
else
    echo "Please provide a correct input"
fi
