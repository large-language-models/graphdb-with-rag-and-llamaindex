#! /usr/bin/bash

chmod u+x stack_utility.sh dev_stack_utility.sh scripts/prod/create_stack.sh scripts/prod/delete_stack.sh scripts/prod/describe_stack.sh scripts/prod/start_neptune.sh scripts/prod/stop_neptune.sh scripts/dev/create_layer.sh scripts/dev/test.sh
sudo snap install yq
python3.10 -m venv "llm_env"
source llm_env/bin/activate
pip install -r requirements.txt